package controllers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import config.Crude;
import models.ClientModel;

public class ClientController {
    private Crude crude = new Crude();

    public void insert(ClientModel clientModel) { //boolean
        try {
            String sql = "INSERT INTO clients (nom, prenom, carteChifa) VALUES ('"
                    + clientModel.getNom() + "','" + clientModel.getPrenom() + "'," + clientModel.getCarteChifa() + ")";
            
            if (crude.exeCreate(sql)) {
                System.out.println("Client ajouté avec succès!");
            } else {
                System.out.println("Erreur lors de l'ajout du client.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean update(ClientModel clientModel, Integer id) {
        try {
            String sql = "UPDATE clients SET nom='" + clientModel.getNom() + "', prenom='" + clientModel.getPrenom()
                    + "', carteChifa=" + clientModel.getCarteChifa() + " WHERE id=" + id;
            return crude.exeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(Integer id) {
        try {
            String sql = "DELETE FROM clients WHERE id=" + id;
            return crude.exeDelete(sql);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<ClientModel> getAll() {
        List<ClientModel> clientModels = new ArrayList<>();
        try {
            String sql = "SELECT * FROM clients";
            ResultSet resultSet = crude.exeRead(sql);
            while (resultSet.next()) {
                ClientModel clientModel = new ClientModel();
                clientModel.setNom(resultSet.getString("nom"));
                clientModel.setPrenom(resultSet.getString("prenom"));
                clientModel.setCarteChifa(resultSet.getInt("carteChifa"));
                clientModels.add(clientModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientModels;
    }

    public ClientModel findById(int id) {
        try {
            String sql = "SELECT * FROM clients WHERE id=" + id;
            ResultSet resultSet = crude.exeRead(sql);
            if (resultSet.next()) {
                ClientModel clientModel = new ClientModel();
                clientModel.setNom(resultSet.getString("nom"));
                clientModel.setPrenom(resultSet.getString("prenom"));
                clientModel.setCarteChifa(resultSet.getInt("carteChifa"));
                return clientModel;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ClientModel findByCarteChifa(int carteChifa) {
        try {
            String sql = "SELECT * FROM clients WHERE carteChifa=" + carteChifa;
            ResultSet resultSet = crude.exeRead(sql);
            if (resultSet.next()) {
                ClientModel clientModel = new ClientModel();
                clientModel.setNom(resultSet.getString("nom"));
                clientModel.setPrenom(resultSet.getString("prenom"));
                clientModel.setCarteChifa(resultSet.getInt("carteChifa"));
                return clientModel;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
