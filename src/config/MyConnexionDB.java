package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyConnexionDB {

	private String url ="jdbc:mysql://localhost:3306/db_pharmacie";
	
	private static MyConnexionDB instance;
	
	private static Connection connection;

	private MyConnexionDB() {
		try {
			/**
			 * Le constructeur étant prive 
			 * Le constructeur se charge de :
			 * 1. Charger le Driver (pilote) en mémoire
			 * 2. Créer l'objet de connexion à la base avec les variables déclarées ci dessus 
             */
			 Properties props = new Properties();
             props.setProperty("user", "root");
             props.setProperty("password", "");
             props.setProperty("characterEncoding", "UTF-8");
             
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, props);

		} catch (Exception e) {
            Logger.getLogger(MyConnexionDB.class.getName()).log(Level.SEVERE, null, e);
		} 
	}
	
	public static MyConnexionDB getInstance() {
		if (instance == null) {
			instance = new MyConnexionDB();
		}
		return instance;
	}
	
	public static Connection getConnection() {
		return connection;
	}
}
