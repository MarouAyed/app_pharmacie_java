package config;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Statement;

public class Crude {

	MyConnexionDB mc = MyConnexionDB.getInstance();
	
	public boolean exeCreate(String sql) {
		try {
			Statement statement = mc.getConnection().createStatement();
			statement.executeUpdate(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean exeUpdate(String sql) {
		try {
			Statement statement = mc.getConnection().createStatement();
			statement.executeUpdate(sql);
			return true;
		} catch (Exception ex) {
            Logger.getLogger(MyConnexionDB.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
	}
	public boolean exeDelete(String sql) {
		try {
			Statement statement = mc.getConnection().createStatement();
			statement.executeUpdate(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public ResultSet exeRead(String sql) {
		try {
			Statement statement = mc.getConnection().createStatement();
			ResultSet rs;
			rs = statement.executeQuery(sql);
			return rs;
		} catch (Exception e) {
			return null;
		}
	}
}
