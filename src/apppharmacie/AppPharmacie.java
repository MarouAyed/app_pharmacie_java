package apppharmacie;

import javax.swing.*;

import views.Menu_principal;

public class AppPharmacie {
    private JFrame frame;
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Client c1 = new Client("Maroua", "AYED",12345);
		//Client c2 = new Client();
		//System.out.println(c1.getNom());
		//System.out.println(c2.toString());
		/**System.out.println(c1.toString());
		System.out.println("-------------------");

		Produit medicament1 = new Medicament("medic001", 12, 3, true, true);
		System.out.println(medicament1.toString());
		System.out.println("-------------------");

		Produit parapharmaceutique1 = new Parapharmaceutique("paraphar001", 12, 3, TypeEnum.BEAUTE);
		System.out.println(parapharmaceutique1.toString());
		System.out.println("-------------------");

		Pharmacie.addClient(c1);
		Pharmacie.addProduit(medicament1);
		Pharmacie.addProduit(parapharmaceutique1);

		Pharmacie.afficherProduits();
		Pharmacie.approvisonner(56, medicament1);
		Pharmacie.approvisonner(10, parapharmaceutique1);
		Pharmacie.afficherProduits();
		
		Pharmacie.vendre(parapharmaceutique1, c1, 1);
		Pharmacie.afficherVentes();
		*/	
	    
		  SwingUtilities.invokeLater(() -> {
	            try {
	                AppPharmacie window = new AppPharmacie();
	                window.initialize();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        });
	    }

	    public AppPharmacie() {
	    	
	    }
	    
	    private void initialize() {
	    	try {
	    		 System.out.print("initialize in AppPharmacie ");
	    		 Menu_principal menuPrincipal = new Menu_principal();
	    	     menuPrincipal.initialize(); // Appeler la méthode initialize() de Menu_principal
			} catch (Exception e) {
				System.out.print("error initialize in AppPharmacie ");
			}	
	    }
}
