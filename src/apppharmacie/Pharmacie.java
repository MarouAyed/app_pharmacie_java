package apppharmacie;

import java.util.*;

import models.ClientModel;
import models.ProduitModel;
import models.VenteModel;

public class Pharmacie {
	
	//si une seule pharmacie donc ajouter static si nn supprimer static
	private static String nom, adresse;
	private static List<ClientModel> mesClient = new ArrayList<ClientModel>(); 
	private static List<ProduitModel> mesProduit = new ArrayList<ProduitModel>();
	private static List<VenteModel> mesVentes = new ArrayList<VenteModel>();

	public static boolean addClient(ClientModel c) { 
		if (Pharmacie.mesClient.contains(c)) {
			System.out.println("Already exists !");
			return false;
		}
		Pharmacie.mesClient.add(c);
			return true;
	}
	
	public static boolean addProduit(ProduitModel p) { 
		if (Pharmacie.mesProduit.contains(p)) {
			System.out.println("Already exists !");
			return false;
		}
		Pharmacie.mesProduit.add(p);
			return true;
	}
	
	
	public static boolean approvisonner(int qty, ProduitModel p) {  //ithafa
		try {
			if (!Pharmacie.mesProduit.contains(p)) {
				System.out.println("Pharmacie does not contain this product");
				return false;
			}
			//will do appro operation
				int index = Pharmacie.mesProduit.indexOf(p); // find the index
				p.setQty(p.getQty()+qty); //  updating qty
				Pharmacie.mesProduit.set(index,p); // updating the item in the list
				return true;
		} catch (Exception ex) {
			System.out.println("Pharmacie class -> approvisonner"+ex.getMessage());
			return false;
		}
	}
	
	public static boolean vendre(ProduitModel p, ClientModel c,int qty) {
		try {
			System.out.println("Vente en cours");
			if (Pharmacie.mesProduit.isEmpty()) {
				System.out.println("Pharmacie is Empty of products");
				return false;
			}
			if (Pharmacie.mesClient.isEmpty()) {
				System.out.println("Pharmacie is Empty of customers");
				return false;
			}
			if (!Pharmacie.mesProduit.contains(p)) {
				System.out.println("Pharmacie does not contain this product");
				return false;
			}
			
			if (!Pharmacie.mesClient.contains(c)) {
				System.out.println("Pharmacie does not contain this client");
				return false;
			}
			
			if (p.getQty()<qty) {
				System.out.println("Pharmacie does not contain the request quantity");
				return false;
			}
			// adjustement of the quantity of the corresponding produit
			int index = Pharmacie.mesProduit.indexOf(p); // find the index
			p.setQty(p.getQty() - qty); //  updating qty
			Pharmacie.mesProduit.set(index,p); // updating the item in the list
	        // adding the item transaction in the list of sales transaction
			VenteModel v = new VenteModel(c,p,qty);
			Pharmacie.mesVentes.add(v);
			
			return true;
	} catch (Exception ex) {
		System.out.println("Pharmacie class -> approvisonner"+ex.getMessage());
		return false;
	}
	}
	
	public static void afficherClients() {
		if (Pharmacie.mesClient.isEmpty()) {
			System.out.println("Empty List Client");
		}else {
			System.out.println("\n here list client");
			for (ClientModel c :Pharmacie.mesClient) {
				System.out.println(c.toString());
			}
		}
	}
	
	public static void afficherProduits() {
		if (Pharmacie.mesProduit.isEmpty()) {
			System.out.println("Empty List Product");
		}else {
			System.out.println("\n here list product");
			for (ProduitModel p :Pharmacie.mesProduit) {
				System.out.println(p.toString());
			}
		}
	}
	public static void afficherVentes() {
		if (Pharmacie.mesVentes.isEmpty()) {
			System.out.println("Empty List Vente");
		}else {
			System.out.println("\n here list vente");
			for (VenteModel v :Pharmacie.mesVentes) {
				System.out.println(v.toString());
			}
		}
	}
}
