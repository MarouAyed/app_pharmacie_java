package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controllers.ClientController;
import models.ClientModel;

public class Client extends JPanel {
    private JPanel currentPanel;

    private JTextField textFieldNom;
    private JTextField textFieldPrenom;
    private JTextField textFieldCarteChifa;
    private ClientController clientController;

    public Client(ClientController clientController) {
        this.clientController = clientController;
        initialize();
    }

    private void initialize() {
        // Initialize the panel
        currentPanel = createClientPanel();
        add(currentPanel, BorderLayout.CENTER);
    }

    private JPanel createClientPanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(new JLabel("Nom:"));

        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(createTextField(textFieldNom = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(new JLabel("Prénom:"));

        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(createTextField(textFieldPrenom = new JTextField()));

        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(new JLabel("Carte Chifa:"));

        gbc.gridx = 1;
        gbc.gridy = 2;
        panel.add(createTextField(textFieldCarteChifa = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        panel.add(createButton("Ajouter Client"));

        return panel;
    }

    private GridBagConstraints makeGbc(int x, int y) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        return gbc;
    }

    
    private JButton createButton(String label) {
        JButton button = new JButton(label);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                handleAddClientButtonClick();
            }
        });
        return button;
    }

    private JTextField createTextField(JTextField textField) {
        textField.setColumns(15);
        textField.setPreferredSize(new Dimension(200, 20)); // Set a preferred size

        return textField;
    }


    private void handleAddClientButtonClick() {
        System.out.println("Add Client button clicked");
        String nom = textFieldNom.getText();
        String prenom = textFieldPrenom.getText();
        String carteChifaText = textFieldCarteChifa.getText();

        // Debug statements
        System.out.println("Nom: " + nom);
        System.out.println("Prénom: " + prenom);
        System.out.println("Carte Chifa: " + carteChifaText);

        try {
            int carteChifa = Integer.parseInt(carteChifaText);

            // Make sure clientController is initialized before calling insert
            if (clientController != null) {
                System.out.println("ClientController is initialized");
                // Create a Client object using the provided data
                ClientModel newClient = new ClientModel(nom, prenom, carteChifa);

                // Pass the newClient object to the insert method
                clientController.insert(newClient);
            } else {
                // Handle the case where clientController is not initialized
                System.out.println("ClientController is not initialized.");
            }
        } catch (NumberFormatException ex) {
            System.out.println("Invalid format for Carte Chifa: " + carteChifaText);
        }
    }

    public void resetClientForm() {
        remove(currentPanel);
        currentPanel = createClientPanel();
        add(currentPanel, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
}
