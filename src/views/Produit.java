package views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Produit extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textFieldRef;
    private JTextField textFieldPrix;
    private JTextField textFieldQte;

    private JPanel currentPanel;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Produit frame = new Produit();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Produit() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        initialize();
    }

    private void initialize() {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu menuProduct = new JMenu("Product");
        menuBar.add(menuProduct);

        JMenuItem menuMedicament = new JMenuItem("Medicament");
        menuMedicament.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showProductForm();
            }
        });
        menuProduct.add(menuMedicament);

        JMenuItem menuParapharmaceutique = new JMenuItem("Parapharmaceutique");
        menuParapharmaceutique.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showProductForm();
            }
        });
        menuProduct.add(menuParapharmaceutique);

        // Ajouter cette ligne pour ajuster la taille de la fenêtre en fonction des composants
        pack();
    }

    private void showProductForm() {
        getContentPane().remove(currentPanel);
        currentPanel = createProductPanel();
        getContentPane().add(currentPanel, BorderLayout.CENTER);
        revalidate();
        repaint();
        pack();
    }

    private JPanel createProductPanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(new JLabel("Ref:"), gbc);

        gbc.gridx = 1;
        panel.add(createTextField(textFieldNom = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(new JLabel("Prix:"), gbc);

        gbc.gridx = 1;
        panel.add(createTextField(textFieldPrenom = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(new JLabel("Qte:"), gbc);

        gbc.gridx = 1;
        panel.add(createTextField(textFieldCarteChifa = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        panel.add(createProduct("Ajouter Product"), gbc);
        panel.setPreferredSize(new Dimension(800, 600));

        return panel;
    }
    


     private JButton createProduct(String label) {
          JButton button = new JButton(label);
          button.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                  String ref = textFieldRef.getText();
                  String prix = textFieldPrix.getText();
                  int qte = Integer.parseInt(textFieldQte.getText());

                  clientController.insert(new Produit(ref, prix, qte));
              }
          });
          return button;
      }
      
      private JTextField createTextField(JTextField textField) {
          textField.setColumns(15);
          return textField;
      }

}

