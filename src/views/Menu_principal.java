package views;

import controllers.ClientController;
import models.ClientModel;

import views.*; 

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.table.DefaultTableModel;

public class Menu_principal {
    private Client clientPanel; // Declare clientPanel at the class level
    private JPanel cards;
    private CardLayout cardLayout;
    
    private JFrame frame;
    
    private JTextField textFieldNom;
    private JTextField textFieldPrenom;
    private JTextField textFieldCarteChifa;
    private JTable table;
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                Menu_principal window = new Menu_principal();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * @wbp.parser.entryPoint
     */
    public Menu_principal() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    public void initialize() {
        System.out.println("Initializing Menu_principal");

        frame = new JFrame();
	    frame.setBounds(100, 100, 899, 572);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    GridBagLayout gridBagLayout = new GridBagLayout();
	    gridBagLayout.columnWidths = new int[]{885, 0};
	    gridBagLayout.rowHeights = new int[]{56, 22, 0, 0, 0, 0, 0, 0};
	    gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	    gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
	    frame.getContentPane().setLayout(gridBagLayout);
	    
	    JPanel panel = new JPanel();
	    panel.setBorder(new LineBorder(new Color(0, 0, 0)));
	    GridBagConstraints gbc_panel = new GridBagConstraints();
	    gbc_panel.fill = GridBagConstraints.BOTH;
	    gbc_panel.insets = new Insets(0, 0, 5, 0);
	    gbc_panel.gridx = 0;
	    gbc_panel.gridy = 0;
	    frame.getContentPane().add(panel, gbc_panel);
	    panel.setLayout(null);
	    
	    JLabel lblNewLabel = new JLabel("Gestion d'une pharmacie 4 éme Ing Génie Logiciel 2023/2024");
	    lblNewLabel.setForeground(new Color(64, 0, 64));
	    lblNewLabel.setFont(new Font("Times New Roman", Font.ITALIC, 30));
	    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    lblNewLabel.setBounds(10, 6, 845, 50);
	    panel.add(lblNewLabel);
	   
                        
	    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
        gbc_tabbedPane.fill = GridBagConstraints.BOTH;
        gbc_tabbedPane.gridx = 0;
        gbc_tabbedPane.gridy = 1;
        frame.getContentPane().add(tabbedPane, gbc_tabbedPane);

        JPanel clientPanel = new JPanel(); // Create a panel for the "Client" tab
        tabbedPane.addTab("Client", null, clientPanel, null);
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        clientPanel.setLayout(new BoxLayout(clientPanel, BoxLayout.X_AXIS));
        clientPanel.add(new JLabel("Nom:"));

        gbc.gridx = 1;
        gbc.gridy = 0;
        clientPanel.add(createTextField(textFieldNom = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        clientPanel.add(new JLabel("Prénom:"));

        gbc.gridx = 1;
        gbc.gridy = 1;
        clientPanel.add(createTextField(textFieldPrenom = new JTextField()));

        gbc.gridx = 0;
        gbc.gridy = 2;
        clientPanel.add(new JLabel("Carte Chifa:"));

        gbc.gridx = 1;
        gbc.gridy = 2;
        clientPanel.add(createTextField(textFieldCarteChifa = new JTextField()), gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        clientPanel.add(createButton("Ajouter Client"));
        
        table = new JTable();
        table.setModel(new DefaultTableModel(
        	new Object[][] {
        	},
        	new String[] {
        		"Id", "Nom", "Prénom", "Carte Chifa"
        	}
        ));
        clientPanel.add(table);
        
        
        JPanel parapharmaceutiquePanel = new JPanel(); 
        tabbedPane.addTab("Parapharmaceutique", null, parapharmaceutiquePanel, null);
        
        JPanel medicamentPanel = new JPanel(); 
        tabbedPane.addTab("Medicament", null, medicamentPanel, null);
        
        JPanel ventePanel = new JPanel(); 
        tabbedPane.addTab("Vente", null, ventePanel, null);
        
        
	    
        // Print a message before setting the frame visible
        System.out.println("About to make the frame visible.");

        // Set the frame visible
        frame.setVisible(true);

        // Print a message after setting the frame visible
        System.out.println("Frame should be visible now.");
    }
    
    
    private JTextField createTextField(JTextField textField) {
        textField.setColumns(15);
        textField.setPreferredSize(new Dimension(200, 20)); // Set a preferred size

        return textField;
    }
    
    private JButton createButton(String label) {
        JButton button = new JButton(label);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //handleAddClientButtonClick();
            }
        });
        return button;
    }
    
   
}
