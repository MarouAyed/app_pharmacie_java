package models;

import java.util.Objects;

public class ProduitModel {
	protected String ref;
	protected double prix;
	protected int qty;
	
	public ProduitModel() {
	}

	public ProduitModel(String ref, double prix, int qty) {
		this.ref = ref;
		this.prix = prix;
		this.qty = qty;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "Produit : ref=" + ref + ", prix=" + prix + ", qty=" + qty;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProduitModel other = (ProduitModel) obj;
		return Objects.equals(ref, other.ref);
	}
	
}