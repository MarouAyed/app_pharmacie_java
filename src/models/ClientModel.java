package models;

//https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.30

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor // Lombok annotation for generating an all-args constructor
@NoArgsConstructor // Lombok annotation for generating a no-args constructor
@Getter // Lombok annotation for generating getters
@Data  // getter and setter and toString

public class ClientModel {
	private String nom, prenom;
	private int carteChifa;
}