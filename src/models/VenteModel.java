package models;

public class VenteModel {
	private ClientModel clientModel;
	private ProduitModel produitModel;
	private int qty;
	
	public VenteModel() {
	}
	
	public VenteModel(ClientModel clientModel, ProduitModel produitModel, int qty) {
		super();
		this.clientModel = clientModel;
		this.produitModel = produitModel;
		this.qty = qty;
	}

	public ClientModel getClient() {
		return clientModel;
	}

	public void setClient(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public ProduitModel getProduit() {
		return produitModel;
	}

	public void setProduit(ProduitModel produitModel) {
		this.produitModel = produitModel;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "Vente [le client=" + clientModel + ", le produit=" + produitModel + ", qty=" + qty + "]";
	}

}
