package models;

public class MedicamentModel extends ProduitModel{
	private boolean estLivreSansOrdonnance;
	private boolean estGenerique;
	
	public MedicamentModel() {
		super();
	}
	
	public MedicamentModel(boolean estLivreSansOrdonnance, boolean estGenerique) {
		super();
		this.estLivreSansOrdonnance = estLivreSansOrdonnance;
		this.estGenerique = estGenerique;
	}

	public MedicamentModel(String ref, double prix, int qty, boolean estLivreSansOrdonnance, boolean estGenerique) {
		super(ref, prix, qty);
		this.estLivreSansOrdonnance = estLivreSansOrdonnance;
		this.estGenerique = estGenerique;
	}

	public boolean isEstLivreSansOrdonnance() {
		return estLivreSansOrdonnance;
	}

	public void setEstLivreSansOrdonnance(boolean estLivreSansOrdonnance) {
		this.estLivreSansOrdonnance = estLivreSansOrdonnance;
	}

	public boolean isEstGenerique() {
		return estGenerique;
	}

	public void setEstGenerique(boolean estGenerique) {
		this.estGenerique = estGenerique;
	}

	@Override
	public String toString() {
		return super.toString() +" est un Medicament [estLivreSansOrdonnance=" + estLivreSansOrdonnance + ", estGenerique=" + estGenerique + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicamentModel other = (MedicamentModel) obj;
		return super.ref == other.ref;
	}
	
}

