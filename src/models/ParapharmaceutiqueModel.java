package models;

enum TypeEnum {BEAUTE, COSMETIQUE, DIETETIQUE}

public class ParapharmaceutiqueModel extends ProduitModel{
	private TypeEnum type;

	public ParapharmaceutiqueModel() {
		super();
	}

	public ParapharmaceutiqueModel(TypeEnum type) {
		super();
		this.type = type;
	}

	public ParapharmaceutiqueModel(String ref, double prix, int qty, TypeEnum type) {
		super(ref, prix, qty);
		this.type = type;
	}

	public TypeEnum getType() {
		return type;
	}

	public void setType(TypeEnum type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return super.toString() +" est un produit Parapharmaceutique de type=" + type;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParapharmaceutiqueModel other = (ParapharmaceutiqueModel) obj;
		return super.ref == other.ref;
	}
	
}
